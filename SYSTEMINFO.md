# Cisco Tetration

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Tetration
Product Page: https://www.cisco.com/c/en_sg/products/data-center-analytics/tetration-analytics/index.html

## Introduction
We classify Cisco Tetration into the Security domain as Cisco Tetration provides access to features and functionalities related to security and workload protection.

"Cisco Tetration offers holistic workload protection for multicloud data centers by enabling a zero-trust model using segmentation" 

## Why Integrate
The Cisco Tetration adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Tetration platform. 

With this adapter you have the ability to perform operations with Cisco Tetration such as:

- List applications
- Get application
- Create application

## Additional Product Documentation
The [API documents for Cisco Tetration](https://www.cisco.com/c/en/us/support/security/tetration/products-installation-and-configuration-guides-list.html)