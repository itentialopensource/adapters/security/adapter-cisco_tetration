
## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!1

---

## 0.1.1 [12-16-2021]

- Initial Commit

See commit 059a64e

---
