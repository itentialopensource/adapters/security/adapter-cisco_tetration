
## 0.4.4 [10-15-2024]

* Changes made at 2024.10.14_20:13PM

See merge request itentialopensource/adapters/adapter-cisco_tetration!13

---

## 0.4.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_tetration!11

---

## 0.4.2 [08-15-2024]

* Changes made at 2024.08.14_18:22PM

See merge request itentialopensource/adapters/adapter-cisco_tetration!10

---

## 0.4.1 [08-06-2024]

* Changes made at 2024.08.06_19:35PM

See merge request itentialopensource/adapters/adapter-cisco_tetration!9

---

## 0.4.0 [05-14-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!8

---

## 0.3.4 [03-27-2024]

* Changes made at 2024.03.27_13:41PM

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!7

---

## 0.3.3 [03-13-2024]

* Changes made at 2024.03.13_11:10AM

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!6

---

## 0.3.2 [03-11-2024]

* Changes made at 2024.03.11_16:14PM

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!4

---

## 0.3.1 [02-27-2024]

* Changes made at 2024.02.27_11:48AM

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!3

---

## 0.3.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!2

---

## 0.2.0 [05-29-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-cisco_tetration!1

---

## 0.1.1 [12-16-2021]

- Initial Commit

See commit 059a64e

---
